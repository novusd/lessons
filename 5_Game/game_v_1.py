from nim_engine_v_1 import put_stones, get_bunches, is_gameover, take_from_bunch

put_stones()

while True:
    print('Текущая позиция')
    print(get_bunches())
    pos = input('Откуда берем?')
    qua = input('Сколько берем?')
    a = take_from_bunch(position=int(pos), quantity=int(qua))
    if a:
        print("Возможно")
    else:
        print('Невозможный ход')

    if is_gameover():
        break
