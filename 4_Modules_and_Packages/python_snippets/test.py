# Внутри модулей пакета можно делать т.н. относительные импорты (см package_1.module_3)
# from package_1.module_3 import function4
# function4()

# # см package_1.subpackage.module_4
from package_1.subpackage.module_4 import function5
function5()

